# -*- coding: utf-8 -*-
"""
Created on Fri Aug 20 16:06:13 2021

@author: ajzwart

This module contains the KaggleOptiverVol Class
that allows the user to import different functions
used during the project
"""

import numpy as np



class KaggleOptiverVol:
    '''
    The Class KaggleOptiverVol is able to processes the forecast of realized
    volatility.

    Attributes
    ----------
        download_status : NoneType initially, str if download_zip
        method is called.

        data_frame : NoneType initially, pandasdataframe when the csv file 
        is unpacked.

        open_csv_statement : NoneType initially, str when a 
        error occurs.

    Methods
    ----------
        import_data : Imports the data provided by optiver of Kaggle 
        via the Kaggle API. 

        submission : Uploaed the given notebook to the Kaggle platform.
        
        plot_corr_matrix : plots correlation matrix for the variables:
        month, humidity, weather situation, temperature, windspeed, 
        and total number of bike rentals.

        week_plotting : plots the distribution of number, 
        of cyclists in a given week chosen by the user.
        
        plot_bar_chart : plots the average of the total rentals by month

        forecast : the forecast method will calculate and plot the mean and
        standard deviation for the defined month.
    '''


    def __init__(self, df=None):
        self.import_data = import_data
        self.submission  = submission
        self.wap         = wap
        self.log_ret     = log_ret
        self.rel_volatility = rel_volatility
                
        
    def import_data(self):
        """
        the import_data function downloads the 
        optiver-realized-volatility-prediction dataset from the Kaggle API.
        
        Parameters
        -----------
        None
  
        Returns
        --------
        Dataset in .parquet format
        """
        !kaggle competitions download -c optiver-realized-volatility-prediction
        
    def wap(self):
        """
        The wap functions calculate the weighted average price 
        of the feeded security
        
        Parameters
        -----------
        df : pandas dataframe
  
        Returns
        --------
        wap : pandas series
        """
                
        wap = (df['bid_price1'] * df['ask_size1'] +
               df['ask_price1'] * df['bid_size1']) / 
                (df['bid_size1']+ df['ask_size1'])
        return wap
    
    
    def log_ret(self):
        """
        The log ret function calculate the logarithmic returns. 
        
        Parameters
        -----------
        df : pandas dataframe
  
        Returns
        --------
        log_ret : numpy series
        """
        list_stock_prices = x
        return np.log(list_stock_prices).diff() 
    
    def rel_volatility(self):
        """
        The log ret function calculate the logarithmic returns. 
        
        Parameters
        -----------
        df : pandas dataframe
  
        Returns
        --------
        rel_volatility : numpy series
        """
        return np.sqrt(np.sum(log_ret**2))


        
    def submission(self):
        """
        Export the notebooks to the Kaggle platform
        
        Parameters
        -----------
        None
  
        Returns
        --------
        None
        """       
        #!kaggle competitions submit -c  optiver-realized-volatility-prediction -f [FILE] -m [MESSAGE]
        