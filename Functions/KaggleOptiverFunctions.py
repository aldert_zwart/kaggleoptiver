# -*- coding: utf-8 -*-
"""
Created on Fri Aug 20 16:06:13 2021

@author: ajzwart

This module contains the KaggleOptiverVol Class
that allows the user to import different functions
used during the project
"""

import numpy as np
import pandas as pd


class KaggleOptiverData:
    '''
    The Class KaggleOptiverData is able to processes the data of 
    the Kaggle project in order toforecast of realized volatility.

    Attributes
    ----------
        download_status : NoneType initially, str if download_zip
        method is called.

        data_frame : NoneType initially, pandasdataframe when the csv file 
        is unpacked.

        open_csv_statement : NoneType initially, str when a 
        error occurs.

    Methods
    ----------
        import_data : Imports the data provided by optiver of Kaggle 
        via the Kaggle API. 

        submission : Uploaed the given notebook to the Kaggle platform.

    '''


    def __init__(self, df=None, train=None):
        self.df = df
        self.train = train
    
    def import_data(self):
        """
        the import_data function downloads the 
        optiver-realized-volatility-prediction dataset from the Kaggle API.
        
        Parameters
        -----------
            None
  
        Returns
        --------
            Dataset in .parquet format
        """
        print("Test")

    def get_train_test_data(self):
        """
        feeds the train & test data to a pandas dataframe, added a key_id 
        in order to merge on book and trade data 
        
        Parameters
        -----------
            None
  
        Returns
        --------
            train, test : pandas dataframe
        """
        train = pd.read_csv('data/train.csv')
        test = pd.read_csv('data/test.csv')
        train['key_id'] = train['stock_id'].astype(str)+'-'+train['time_id'].astype(str)
        test['key_id'] = test['stock_id'].astype(str)+'-'+test['time_id'].astype(str) 
        return train, test

    def get_stock_id(self, train):
        """
        feeds the train & test data to a pandas dataframe, added a key_id 
        in order to merge on book and trade data 
        
        Parameters
        -----------
            Train : pandas dataframe
  
        Returns
        --------
            unique_stock_id : Pandas serie
        """
        unique_stock_id = train['stock_id'].unique()
        return unique_stock_id

    def wap(self, df):
        """
        The wap functions calculate the weighted average price 
        of the feeded security
            
        Parameters
        -----------
            df : pandas dataframe
  
        Returns
        --------
            df : pandas series
        """
        df['wap1'] =  (df['bid_price1'] * df['ask_size1'] + df['ask_price1'] * df['bid_size1']) / (df['bid_size1'] + df['ask_size1'])
        df['wap2'] =  (df['bid_price2'] * df['ask_size2'] + df['ask_price2'] * df['bid_size2']) / (df['bid_size2'] + df['ask_size2'])
        df['stock_id'] = df['stock_id']
        df['time_id'] = df['time_id']
        return df

    def log_ret(self, df):
        """
        The log ret function calculate the logarithmic returns. 
        
        Parameters
        -----------
            df : pandas dataframe
  
        Returns
        --------
            df
        """
        df['ret1'] = np.log(df['wap1']).diff() 
        df['ret2'] = np.log(df['wap2']).diff() 
        df['stock_id'] = df['stock_id']
        df['time_id'] = df['time_id']
        return df
 
    def rel_volatility(self, df):
        """
        The log ret function calculate the logarithmic returns. 
        
        Parameters
        -----------
        df : pandas dataframe
  
        Returns
        --------
        df_vol : pandas DataFrame
        """
        df['rel_vol1'] = np.sqrt(np.sum(df['ret1']**2))
        df['rel_vol2'] = np.sqrt(np.sum(df['ret2']**2))
        df['stock_id'] = df['stock_id']
        df['time_id'] = df['time_id']
        return df

    def spread(self, df):
        """
        The spread function calculates different types of spreads. 
        
        Parameters
        -----------
        df : pandas dataframe
  
        Returns
        --------
        df: pandas DataFrame  
        """
        df['price_spread'] = (df['ask_price1'] - df['bid_price1']) / ((df['ask_price1'] + df['bid_price1']) / 2)
        df['price_spread2'] = (df['ask_price2'] - df['bid_price2']) / ((df['ask_price2'] + df['bid_price2']) / 2)
        df['bid_spread'] = df['bid_price1'] - df['bid_price2']
        df['ask_spread'] = df['ask_price1'] - df['ask_price2']
        df['bid_ask_spread'] =  abs(df['bid_spread'] - df['ask_spread'])
        df['stock_id'] = df['stock_id']
        df['time_id'] = df['time_id']
        return df

    def volume(self, df):
        """
        The volume function calculates the volume from the dataset
        
        Parameters
        -----------
        df : pandas dataframe
  
        Returns
        --------
        df : pandas DataFrame
        """
        df['total_volume'] = (df['ask_size1'] + df['ask_size2']) + (df['bid_size1'] + df['bid_size2'])
        df['volume_imbalance'] =  abs((df['ask_size1'] + df['ask_size2']) - df['bid_size1'] + df['bid_size2'])
        df['stock_id'] = df['stock_id']
        df['time_id'] = df['time_id']
        return df

    def get_book_data(self, stock_id : int, type: int):
        """
        Get the book and trade dataset
        
        Parameters
        -----------
            None
  
        Returns
        --------
            df_book : pandas DataFrame
        """
        df_book = pd.read_parquet(f"data/book_{type}.parquet/stock_id={stock_id}", engine='pyarrow')
        df_book = df_book.sort_values(by=['time_id', 'seconds_in_bucket']).reset_index(drop=True)
        df_book['stock_id'] = stock_id
        df_book = self.wap(df_book)
        df_book = self.log_ret(df_book)
        df_book = self.spread(df_book)
        df_book = self.volume(df_book)
        return df_book

    def get_trade_data(self, stock_id : int, type : int):
        """
        Get the book and trade dataset
        
        Parameters
        -----------
            None
  
        Returns
        --------
            df
        """
        df_trade = pd.read_parquet(f"data/trade_{type}.parquet/stock_id={stock_id}", engine='pyarrow')
        df_trade = df_trade.sort_values(by=['time_id', 'seconds_in_bucket']).reset_index(drop=True)
        df_trade['stock_id'] = stock_id
        return df_trade

    def merge_dataset(self, df_book, df_trade):
        """
        Merge the trade & book dataframes
        
        Parameters
        -----------
            df_book, df_trade
  
        Returns
        --------
            merged dataset
        """
        merged_df = df_book.merge(df_trade, on=['stock_id', 'time_id'], how='left')
        return merged_df

    def final_dataset(self, unique_stock_id, type, train_df):
        """
        Merge the trade & book dataframes and interate over the different securities
        
        Parameters
        -----------
            unique_stock_id : list and type : int
  
        Returns
        --------
            train_df : pandas dataframe
        """
        for stock in unique_stock_id:
            print(f'Data processing of stock: {stock}')
            df_book = self.get_book_data(stock, type)
            df_trade = self.get_trade_data(stock, type)
            merged_df = self.merge_dataset(df_book, df_trade)
            train_df = train_df.merge(merged_df, on=['stock_id', 'time_id'], how='left')
            print(f'Processed stock: {stock}')
        return train_df

    def submission(self):
        """
        Export the notebooks to the Kaggle platform
        
        Parameters
        -----------
        None
  
        Returns
        --------
        None
        """       
        #!kaggle competitions submit -c  optiver-realized-volatility-prediction -f [FILE] -m [MESSAGE]
        
